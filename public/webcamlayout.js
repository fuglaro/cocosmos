/**
 * Arranges the layout of the WebcamPane.
 * 
 * Finds the element with id "webcamPane" and arranges its
 * children of class "webcam" in an optimal way.
 * The arangement is made so that all webcams are forced
 * to an aspect ratio of 1, and given the largest
 * equal sizes. They are then spread out across centered
 * rows.
 * 
 * To center fit the ".webcam" widgets, give their css styles
 * the following values:
 *      object-fit: cover;
 *      position: absolute;
 */
class WebCamLayout {
    constructor() {
        this.panel = document.querySelector("#webcamPane");
        this.layout();
    }

    /**
     * Reset the layout.
     * 
     * Ensure this is called whenever the "#webcamPane"
     * widget is resized.
     */
    layout() {
        var width = this.panel.offsetWidth;
        var height = this.panel.offsetHeight;
        var cams = document.querySelectorAll("#webcamPane > .webcam");

        // Find the number of rows needed to stack the cameras.
        var rows = 0, cols = 0;
        do {
            rows += 1;
            cols = Math.ceil(cams.length / rows);
            var camWidth = width / cols
        } while (camWidth < height / (rows + 1));
        // Fit based on tightest constraint, width or height.
        camWidth = Math.min(camWidth, height / Math.ceil(rows));
        // Find indicator of which rows are different lengths.
        var remainder = cams.length % rows;

        // Walk across the panel, dropping webcam widget positions.
        var vertOffset = (height - (rows * camWidth)) / 2;
        var offset = (width - (cols * camWidth)) / 2;
        // When some rows are shorter, there is a larger offset.
        var longOffset = (width - ((cols - 1) * camWidth)) / 2;
        var row = 0, col = 0;
        cams.forEach(function (cam) {

            // Set the cam widget's position.
            // Decide between long or short offset.
            if (!remainder || row < remainder) {
                cam.style.left = `${col*camWidth + offset}px`;
            } else {
                cam.style.left = `${col*camWidth + longOffset}px`;
            }
            cam.style.top = `${row*camWidth + vertOffset}px`;
            cam.style.width = `${camWidth}px`;
            // Cam widgets are always square - no ifs, no buts.
            cam.style.height = `${camWidth}px`;

            // Find the next placement position.
            row += 1;
            if (row >= rows) {
                row = 0;
                col += 1;
            }
        }); 
    }
}