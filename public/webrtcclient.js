
// THIS FILE IS FOR TESTING.

// This connects the webcam to the webcam elements.
// This is a place holder for a WebRTC chatroom. 

function hasUserMedia() { 
    // Check if the browser supports the WebRTC.
    return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || 
       navigator.mozGetUserMedia); 
}

if (hasUserMedia()) { 
   navigator.getUserMedia = navigator.getUserMedia
      || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

   // Connect all the webcam channels.
   navigator.getUserMedia({
         video: {
            facingMode: "user",
            width: { ideal: 160 },
            height: { ideal: 160 }},
         audio: false
      },
      function (stream) {
         document.querySelectorAll('video').forEach(function (video) {
            video.srcObject = stream;
         });
      },
      function (err) {
         console.log(err)
      });
} else { 
   alert("WebRTC is not supported"); 
}